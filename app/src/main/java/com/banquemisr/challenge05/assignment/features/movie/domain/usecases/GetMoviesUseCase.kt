package com.banquemisr.challenge05.assignment.features.movie.domain.usecases

import androidx.paging.PagingData
import com.banquemisr.challenge05.assignment.features.movie.data.model.request.MovieQuery
import com.banquemisr.challenge05.assignment.features.movie.domain.entities.MovieResultEntity
import com.banquemisr.challenge05.assignment.features.movie.domain.repositories.MovieRepository
import kotlinx.coroutines.flow.Flow

class GetMoviesUseCase(
    private val repository: MovieRepository
) {
    operator fun invoke(
        query: MovieQuery
    ): Flow<PagingData<MovieResultEntity>> {
        return repository.getMovieList(query)
    }
}