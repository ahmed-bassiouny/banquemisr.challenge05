package com.banquemisr.challenge05.assignment.features.movie.domain.usecases

import com.banquemisr.challenge05.assignment.core.utils.Resource
import com.banquemisr.challenge05.assignment.core.utils.responseFlow
import com.banquemisr.challenge05.assignment.features.movie.domain.entities.DetailMovieEntity
import com.banquemisr.challenge05.assignment.features.movie.domain.repositories.MovieRepository
import kotlinx.coroutines.flow.Flow

class GetDetailMoviesByIdUseCase(
    private val repository: MovieRepository
) {
    operator fun invoke(
        movieID: Int
    ): Flow<Resource<DetailMovieEntity>> {
        return responseFlow { repository.getMovieDetail(movieID) }
    }
}