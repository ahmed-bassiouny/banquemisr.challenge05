package com.banquemisr.challenge05.assignment.ui.theme

import androidx.compose.ui.graphics.Color

val blue = Color(0xFF073D92)
val darkBlue = Color(0xFF133080)
val Pink80 = Color(0xFFEFB8C8)

