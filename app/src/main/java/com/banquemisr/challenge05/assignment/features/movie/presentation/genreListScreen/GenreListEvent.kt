package com.banquemisr.challenge05.assignment.features.movie.presentation.genreListScreen

import com.banquemisr.challenge05.assignment.features.movie.domain.entities.GenreEntity

sealed interface GenreListEvent {
    data class NavigateToMovieList(
        val genre: GenreEntity
    ) : GenreListEvent
    data object GetGenres : GenreListEvent
}