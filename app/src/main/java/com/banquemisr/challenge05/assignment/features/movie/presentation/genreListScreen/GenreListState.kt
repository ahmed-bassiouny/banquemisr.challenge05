package com.banquemisr.challenge05.assignment.features.movie.presentation.genreListScreen

import com.banquemisr.challenge05.assignment.features.movie.domain.entities.GenreEntity

data class GenreListState(
    val isLoading: Boolean = false,
    val genresResult: Pair<Boolean, List<GenreEntity>?> = Pair(false, emptyList())
)