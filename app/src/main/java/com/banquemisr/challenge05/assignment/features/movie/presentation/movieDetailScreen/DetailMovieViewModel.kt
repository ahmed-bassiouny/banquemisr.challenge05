package com.banquemisr.challenge05.assignment.features.movie.presentation.movieDetailScreen

import com.banquemisr.challenge05.assignment.core.presentation.UiEvent
import com.banquemisr.challenge05.assignment.core.utils.Resource.Error
import com.banquemisr.challenge05.assignment.core.utils.Resource.Loading
import com.banquemisr.challenge05.assignment.core.utils.Resource.Success
import com.banquemisr.challenge05.assignment.core.viewModel.BaseViewModel
import com.banquemisr.challenge05.assignment.features.movie.domain.entities.MovieResultEntity
import com.banquemisr.challenge05.assignment.features.movie.domain.usecases.GetDetailMoviesByIdUseCase
import com.banquemisr.challenge05.assignment.features.movie.domain.usecases.GetReviewByMovieIdUseCase
import com.banquemisr.challenge05.assignment.features.movie.domain.usecases.GetVideoByMovieIdUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class DetailMovieViewModel @Inject constructor(
    private val getDetailMovie: GetDetailMoviesByIdUseCase,
    private val getReviewers: GetReviewByMovieIdUseCase,
    private val getTrailers: GetVideoByMovieIdUseCase,
) : BaseViewModel<DetailMovieState, DetailMovieEvent>() {
    override fun defaultState(): DetailMovieState = DetailMovieState()

    override fun onEvent(event: DetailMovieEvent) {
        when (event) {
            is DetailMovieEvent.SetMovie -> onSetMovie(event.movie)
            is DetailMovieEvent.GetDetailMovie -> onGetDetailMovie()


            is DetailMovieEvent.NavigateBack -> sendEvent(
                UiEvent.NavigateBack
            )

        }
    }

    private fun onSetMovie(movie: MovieResultEntity?) = commit {
        copy(movie = movie)
    }




    private fun onGetDetailMovie() = asyncWithState {
        movie?.id?.let { getDetailMovie(it) }?.collect { result ->
            when (result) {
                is Loading -> commit {
                    copy(
                        isFetchingDetail = true
                    )
                }

                is Success -> {
                    commit {
                        copy(
                            detailMovie = result.data,
                            isFetchingDetail = false
                        )
                    }
                    Timber.d("onGetDetailMovie: ${result.data}")
                }

                is Error -> {
                    commit {
                        copy(
                            detailMovie = null,
                            isFetchingDetail = false
                        )
                    }
                    sendEvent(UiEvent.ShowSnackBar(result.message))
                    Timber.d("onGetDetailMovie Error: ${result.message}")
                }
            }

        }
    }

}