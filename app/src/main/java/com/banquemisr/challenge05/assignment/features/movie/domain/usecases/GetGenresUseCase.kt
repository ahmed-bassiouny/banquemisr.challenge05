package com.banquemisr.challenge05.assignment.features.movie.domain.usecases

import com.banquemisr.challenge05.assignment.core.utils.Resource
import com.banquemisr.challenge05.assignment.core.utils.responseFlow
import com.banquemisr.challenge05.assignment.features.movie.domain.entities.GenreEntity
import com.banquemisr.challenge05.assignment.features.movie.domain.repositories.MovieRepository
import kotlinx.coroutines.flow.Flow

class GetGenresUseCase(
    private val repository: MovieRepository
) {
     operator fun invoke(): Flow<Resource<List<GenreEntity>>> {
        return responseFlow { repository.getMovieGenre() }
    }
}