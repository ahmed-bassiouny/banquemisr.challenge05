package com.banquemisr.challenge05.assignment.features.movie.presentation.movieDetailScreen

import com.banquemisr.challenge05.assignment.features.movie.domain.entities.DetailMovieEntity
import com.banquemisr.challenge05.assignment.features.movie.domain.entities.MovieResultEntity

data class DetailMovieState(
    val movie : MovieResultEntity? = null,
    val isFetchingDetail: Boolean = false,
    val detailMovie : DetailMovieEntity? = null,
)