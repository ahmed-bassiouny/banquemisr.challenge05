@file:OptIn(ExperimentalMaterial3Api::class)

package com.banquemisr.challenge05.assignment.features.movie.presentation.movieDetailScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import com.banquemisr.challenge05.assignment.core.utils.Constants
import com.banquemisr.challenge05.assignment.core.utils.extension.Empty
import com.banquemisr.challenge05.assignment.ui.components.ErrorContainer
import com.banquemisr.challenge05.assignment.ui.components.LoadingContainer
import com.banquemisr.challenge05.assignment.ui.theme.AppTheme


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun DetailMovieScreen(
    state: DetailMovieState,
    onEvent: (DetailMovieEvent) -> Unit,
) {
    val movie = state.detailMovie

    LaunchedEffect(Unit) {
        onEvent(DetailMovieEvent.GetDetailMovie)
    }
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = state.movie?.title.orEmpty(),
                        style = typography.titleLarge,
                    )
                },
                navigationIcon = {
                    IconButton(onClick = {
                        onEvent(DetailMovieEvent.NavigateBack)
                    }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = String.Empty
                        )
                    }
                },
                colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = Color.White)

            )
        }
    ) { innerPadding ->
        if (state.isFetchingDetail) {
            LoadingContainer()
        } else if (movie == null) {
            ErrorContainer {
                onEvent(DetailMovieEvent.GetDetailMovie)
            }
        } else {
            val painter = rememberAsyncImagePainter(
                Constants.BASE_IMAGE_URL + movie.posterPath,
            )
            LazyColumn(
                modifier = Modifier
                    .padding(innerPadding)
                    .padding(horizontal = 16.dp)
                    .background(Color.White)
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.Start
            ) {
                item {
                    if (!movie.posterPath.isNullOrBlank()) {
                        Image(
                            painter = painter,
                            contentDescription = null,
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(300.dp)
                                .clip(shape = RoundedCornerShape(8.dp))
                        )
                    }

                    Spacer(modifier = Modifier.height(16.dp))

                    Text(
                        text = "Overview:",
                        style = typography.titleLarge
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = movie.overview.orEmpty(),
                        style = typography.bodyLarge,
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = "Release Date:",
                        style = typography.titleLarge
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = movie.releaseDate.orEmpty(),
                        style = typography.bodyLarge,
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = "Popularity:",
                        style = typography.titleLarge
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = movie.popularity?.toString().orEmpty(),
                        style = typography.bodyLarge,
                    )

                    Spacer(modifier = Modifier.height(16.dp))
                    Text(
                        text = "Vote Average:",
                        style = typography.titleLarge
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = movie.voteAverage?.toString().orEmpty(),
                        style = typography.bodyLarge,
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(
                        text = "Production Companies:",
                        style = typography.titleLarge
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    movie.productionCompanies.forEach { company ->
                        Text(
                            text = "- ${company.name.orEmpty()}",
                            style = typography.bodyLarge,
                            modifier = Modifier.padding(start = 16.dp)
                        )
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    Text(
                        text = "Production Countries:",
                        style = typography.titleLarge
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    movie.productionCountries.forEach { country ->
                        Text(
                            text = "- ${country.name.orEmpty()}",
                            style = typography.bodyLarge,
                            modifier = Modifier.padding(start = 16.dp)
                        )
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                    Text(
                        text = "Spoken Languages:",
                        style = typography.titleLarge,
                    )
                    Spacer(modifier = Modifier.height(8.dp))
                    movie.spokenLanguages.forEach { language ->
                        Text(
                            text = "- ${language.name.orEmpty()}",
                            style = typography.bodyLarge,
                            modifier = Modifier.padding(start = 16.dp)
                        )
                    }
                    Spacer(modifier = Modifier.height(16.dp))
                }

            }
        }
    }
}


@Preview(showBackground = true)
@Composable
private fun Preview() {
    AppTheme {
        DetailMovieScreen(
            state = DetailMovieState(),
            onEvent = {}
        )
    }
}