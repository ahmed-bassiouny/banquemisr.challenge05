package com.banquemisr.challenge05.assignment.features.movie.presentation.movieListScreen

import androidx.paging.PagingData
import com.banquemisr.challenge05.assignment.features.movie.domain.entities.GenreEntity
import com.banquemisr.challenge05.assignment.features.movie.domain.entities.MovieResultEntity
import kotlinx.coroutines.flow.Flow

data class MovieListState(
    val genre : GenreEntity? = null,
    val movies : Flow<PagingData<MovieResultEntity>>? = null,
)