@file:OptIn(ExperimentalMaterial3Api::class)

package com.banquemisr.challenge05.assignment.features.movie.presentation.movieListScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import androidx.paging.compose.itemKey
import coil.compose.rememberAsyncImagePainter
import com.banquemisr.challenge05.assignment.core.utils.Constants.Companion.BASE_IMAGE_URL
import com.banquemisr.challenge05.assignment.core.utils.extension.Empty
import com.banquemisr.challenge05.assignment.features.movie.domain.entities.MovieResultEntity
import com.banquemisr.challenge05.assignment.ui.components.ErrorContainer
import com.banquemisr.challenge05.assignment.ui.components.LoadingContainer
import com.banquemisr.challenge05.assignment.ui.theme.AppTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieListScreen(
    state: MovieListState,
    onEvent: (MovieListEvent) -> Unit,
) {
    val movies = state.movies?.collectAsLazyPagingItems()


    LaunchedEffect(Unit) {
        onEvent(MovieListEvent.GetMovies)
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Movie ${state.genre?.name ?: String.Empty}",
                        style = typography.titleLarge,
                    )
                },
                navigationIcon = {
                    IconButton(onClick = {
                        onEvent(MovieListEvent.NavigateBack)
                    }) {
                        Icon(
                            imageVector = Icons.Filled.ArrowBack,
                            contentDescription = String.Empty
                        )
                    }
                },
                colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = Color.White)
            )
        }
    ) { innerPadding ->
        Box(
            modifier = Modifier
                .background(Color.White)
                .fillMaxSize()
                .padding(innerPadding)
                .padding(horizontal = 6.dp)
        ) {
            when (movies?.loadState?.refresh) {
                is LoadState.Error -> {
                    ErrorContainer {
                        onEvent(MovieListEvent.GetMovies)
                    }
                }

                is LoadState.Loading -> {
                    LoadingContainer()
                }

                else -> {
                    LazyVerticalGrid(
                        modifier = Modifier.fillMaxSize(),
                        columns = GridCells.Fixed(1),
                        contentPadding = PaddingValues(1.dp)
                    ) {
                        items(
                            count = movies?.itemCount ?: 0,
                            key = movies?.itemKey { it.id.toString() },
                        ) {
                            movies?.get(it)?.let { movie ->
                                MovieCard(
                                    movie = movie,
                                    onClick = {
                                        onEvent(MovieListEvent.NavigateToDetail(movie))
                                    }
                                )
                            }
                        }
                        when (movies?.loadState?.append) {
                            is LoadState.Error -> {
                                item {
                                    ErrorContainer {
                                        movies.retry()
                                    }
                                }
                            }

                            is LoadState.Loading -> {
                                item {
                                    Column(
                                        modifier = Modifier
                                            .fillMaxWidth(),
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        verticalArrangement = Arrangement.Center,
                                    ) {
                                        CircularProgressIndicator()
                                    }
                                }
                            }

                            else -> {
                                // DO NOTHING
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun MovieCard(
    movie: MovieResultEntity,
    onClick: () -> Unit,
) {
    val painter = rememberAsyncImagePainter(
        BASE_IMAGE_URL + movie.posterPath,
    )
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(8.dp),
        onClick = onClick
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.LightGray)

        ) {
            if (!movie.posterPath.isNullOrBlank()) {
                Image(
                    painter = painter,
                    contentDescription = "Movie Poster",
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(200.dp)
                        .clip(shape = RoundedCornerShape(8.dp)),
                    contentScale = ContentScale.FillBounds
                )
            }
            Spacer(modifier = Modifier.height(8.dp))
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp)
            ) {
                Text(
                    text = movie.title.orEmpty(),
                    maxLines = 1,
                    style = typography.bodyLarge.copy(fontWeight = FontWeight.Bold),
                    modifier = Modifier
                        .fillMaxWidth()
                )

                Text(
                    text = movie.releaseDate.orEmpty(),
                    style = typography.bodyMedium,
                    modifier = Modifier
                        .fillMaxWidth()
                )
                Spacer(modifier = Modifier.height(4.dp))
            }
        }
    }
}


@Preview(showBackground = true)
@Composable
private fun Preview() {
    AppTheme {
        MovieListScreen(
            state = MovieListState(),
            onEvent = {}
        )
    }
}