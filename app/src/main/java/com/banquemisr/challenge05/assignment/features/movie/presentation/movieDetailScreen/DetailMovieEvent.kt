package com.banquemisr.challenge05.assignment.features.movie.presentation.movieDetailScreen

import com.banquemisr.challenge05.assignment.features.movie.domain.entities.MovieResultEntity

sealed interface DetailMovieEvent {
    data class SetMovie(
        val movie: MovieResultEntity?
    ) : DetailMovieEvent

    data object GetDetailMovie : DetailMovieEvent
    data object NavigateBack : DetailMovieEvent
}