@file:OptIn(ExperimentalMaterial3Api::class)

package com.banquemisr.challenge05.assignment.features.movie.presentation.genreListScreen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.banquemisr.challenge05.assignment.core.utils.extension.Empty
import com.banquemisr.challenge05.assignment.features.movie.domain.entities.GenreEntity
import com.banquemisr.challenge05.assignment.ui.components.ErrorContainer
import com.banquemisr.challenge05.assignment.ui.theme.AppTheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GenreListScreen(
    state: GenreListState,
    onEvent: (GenreListEvent) -> Unit,
) {
    LaunchedEffect(Unit) {
        onEvent(GenreListEvent.GetGenres)
    }


    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text(
                        text = "Choose your genre",
                        style = typography.titleLarge,
                    )
                }, colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = Color.White)
            )
        }
    ) { innerPadding ->
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White)
                .padding(innerPadding)
                .padding(horizontal = 16.dp)
        ) {
            if (!state.genresResult.first && state.genresResult.second == null) {
                ErrorContainer {
                    onEvent(GenreListEvent.GetGenres)
                }
            } else {
                LazyVerticalGrid(
                    modifier = Modifier.fillMaxSize(),
                    columns = GridCells.Fixed(2),
                ) {
                    items(state.genresResult.second?.size ?: 0) {
                        state.genresResult.second?.get(it)?.let { genre ->
                            GenreItem(
                                genre = genre,
                                onClick = {
                                    onEvent(GenreListEvent.NavigateToMovieList(genre))
                                }
                            )
                        }
                    }
                }
            }
        }
    }

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GenreItem(
    genre: GenreEntity,
    onClick: () -> Unit,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(1.dp)
    ) {
        Button(onClick = onClick) {
            Text(
                genre.name ?: String.Empty,
                style = typography.labelSmall,
                modifier = Modifier.padding(2.dp)
            )
        }
    }
}


@Preview(showBackground = true)
@Composable
private fun Preview() {
    AppTheme {
        GenreListScreen(
            state = GenreListState(),
            onEvent = {}
        )
    }
}