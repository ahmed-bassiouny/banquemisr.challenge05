package com.banquemisr.challenge05.assignment

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class Application : Application()