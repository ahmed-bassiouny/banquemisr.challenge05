package com.banquemisr.challenge05.assignment.core.model

open class BaseHeaderItem {
    var requestSignature: String? = null
    var clientId: String? = null
}